﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CommonTasksWebForms._2_Get_And_Set_Values_Of_Form_Elements
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnCopy_Click(object sender, EventArgs e)
        {
            txtDestination.Text = txtSource.Text;
            lblDestination.Text = txtSource.Text;
        }
    }
}