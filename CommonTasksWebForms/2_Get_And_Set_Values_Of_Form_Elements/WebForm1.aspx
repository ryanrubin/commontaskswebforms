﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="CommonTasksWebForms._2_Get_And_Set_Values_Of_Form_Elements.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            Source: <asp:TextBox ID="txtSource" runat="server"></asp:TextBox>
        </div>
        <div>
            <asp:Button ID="btnCopy" runat="server" Text="Copy Textbox Value From Source to Destination" OnClick="btnCopy_Click" />
        </div>
        <div>
            Destination: <asp:TextBox ID="txtDestination" runat="server"></asp:TextBox>
        </div>
        <div>
            Destination Label: <asp:Label ID="lblDestination" runat="server" Text="(No Value)"></asp:Label>
        </div>
    </form>
</body>
</html>
