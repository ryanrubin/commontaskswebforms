﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CommonTasksWebForms._5_Show_And_Hide_Dom_Elements
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnHide_Click(object sender, EventArgs e)
        {
            for (int i = 1; i <= 3; i++)
            {
                ((TextBox)FindControl("TextBox" + i)).Visible = true;
            }

            int randomTextboxNumber = new Random().Next(1, 3 + 1);
            ((TextBox)FindControl("TextBox" + randomTextboxNumber)).Visible = false;
        }
    }
}