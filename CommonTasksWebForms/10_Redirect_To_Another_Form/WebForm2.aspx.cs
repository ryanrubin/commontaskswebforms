﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CommonTasksWebForms._10_Redirect_To_Another_Form
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Label1.Text = Request["q"];
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/10_Redirect_To_Another_Form/WebForm1.aspx?q=This+parameter+is+from+WebForm2");
        }
    }
}