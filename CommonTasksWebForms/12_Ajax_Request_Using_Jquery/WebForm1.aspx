﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="CommonTasksWebForms._12_Ajax_Request_Using_Jquery.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <span id="dateTimeSpan">(No Data)</span>
            <button type="button" id="getDateTimeButton">Get Date/Time via jQuery AJAX</button>
        </div>
    </form>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        $(function () {
            $('#getDateTimeButton').click(function () {
                $.ajax({
                    type: 'POST',
                    url: 'WebForm1.aspx/GetDateTime',
                    data: JSON.stringify({ param: 'This param comes from the client' }),
                    contentType: 'application/json'
                }).done(function (response) {
                    $('#dateTimeSpan').text(response.d);
                });
            });
        });
    </script>
</body>
</html>
