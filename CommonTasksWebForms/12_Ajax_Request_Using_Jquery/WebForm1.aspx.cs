﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CommonTasksWebForms._12_Ajax_Request_Using_Jquery
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [System.Web.Services.WebMethod]
        public static string GetDateTime(string param)
        {
            return param + " | " + DateTime.Now.ToString();
        }
    }
}