﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CommonTasksWebForms._6_Add_And_Remove_Classes_Of_Dom_Elements
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        private bool IsWarningMessage
        {
            get
            {
                return (bool)(ViewState["IsWarningMessage"] ?? false);
            }
            set
            {
                ViewState["IsWarningMessage"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnToggleMessage_Click(object sender, EventArgs e)
        {
            IsWarningMessage = !IsWarningMessage;
            if (IsWarningMessage)
            {
                lblMessage.Text = "This is a warning message.";
                lblMessage.CssClass = "warning-text";
            }
            else
            {
                lblMessage.Text = "This is a normal message.";
                lblMessage.CssClass = "";
            }
        }
    }
}