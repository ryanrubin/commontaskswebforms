﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="CommonTasksWebForms._6_Add_And_Remove_Classes_Of_Dom_Elements.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        .warning-text {
            color: red;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="lblMessage" runat="server" Text="(No Message)"></asp:Label>
        </div>
        <div>
            <asp:Button ID="btnToggleMessage" runat="server" Text="Toggle Message" OnClick="btnToggleMessage_Click" />
        </div>
    </form>
</body>
</html>
