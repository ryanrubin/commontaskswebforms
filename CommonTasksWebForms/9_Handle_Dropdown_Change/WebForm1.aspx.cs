﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CommonTasksWebForms._9_Handle_Dropdown_Change
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        public class Person
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public int Age { get; set; }
            public string Location { get; set; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                List<Person> people = new List<Person>(10);
                people.Add(new Person { Id = 1, Name = "Name 1", Age = 1, Location = "Location 1" });
                people.Add(new Person { Id = 2, Name = "Name 2", Age = 2, Location = "Location 2" });
                people.Add(new Person { Id = 3, Name = "Name 3", Age = 3, Location = "Location 3" });
                DropDownList1.DataValueField = "Id";
                DropDownList1.DataTextField = "Name";
                DropDownList1.DataSource = people;
                DropDownList1.DataBind();
            }
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Label1.Text = DropDownList1.SelectedItem.Value + "-" + DropDownList1.SelectedItem.Text;
        }
    }
}