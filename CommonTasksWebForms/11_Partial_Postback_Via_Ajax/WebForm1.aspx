﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="CommonTasksWebForms._11_Partial_Postback_Via_Ajax.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div>
            Non-ajax timer<br />
            - does full postback<br />
            - browser reloads for every postback
        </div>
        <div>
            <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
            <asp:Timer ID="Timer1" runat="server" Enabled="False" Interval="1000" OnTick="Timer1_Tick"></asp:Timer>
            <asp:Button ID="Button1" runat="server" Text="Start" OnClick="Button1_Click" />
        </div>
        <hr />
        <div>
            Ajax timer<br />
            - does partial postback<br />
            - browser does not reload for every postback
            - need to wrap the server controls in an update panel
        </div>
        <div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>
                    <asp:Timer ID="Timer2" runat="server" Enabled="False" Interval="1000" OnTick="Timer2_Tick"></asp:Timer>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:Button ID="Button2" runat="server" Text="Start" OnClick="Button2_Click" />
        </div>
    </form>
</body>
</html>
