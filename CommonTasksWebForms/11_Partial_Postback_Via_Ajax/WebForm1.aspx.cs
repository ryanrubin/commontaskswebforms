﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CommonTasksWebForms._11_Partial_Postback_Via_Ajax
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (Timer1.Enabled)
            {
                Button1.Text = "Start";
                Timer1.Enabled = false;
            }
            else
            {
                Button1.Text = "Stop";
                Timer1.Enabled = true;

                Button2.Text = "Start";
                Timer2.Enabled = false;
            }
        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {
            Label1.Text = DateTime.Now.ToString();
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            if (Timer2.Enabled)
            {
                Button2.Text = "Start";
                Timer2.Enabled = false;
            }
            else
            {
                Button2.Text = "Stop";
                Timer2.Enabled = true;

                Button1.Text = "Start";
                Timer1.Enabled = false;
            }
        }

        protected void Timer2_Tick(object sender, EventArgs e)
        {
            Label2.Text = DateTime.Now.ToString();
        }
    }
}