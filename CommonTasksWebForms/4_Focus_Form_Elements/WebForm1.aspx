﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="CommonTasksWebForms._4_Focus_Form_Elements.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            TextBox1: <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        </div>
        <div>
            TextBox2: <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
        </div>
        <div>
            TextBox3: <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
        </div>
        <div>
            <asp:Button ID="btnFocus" runat="server" Text="Focus a Random Textbox" OnClick="btnFocus_Click" />
        </div>
    </form>
</body>
</html>
