﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CommonTasksWebForms._4_Focus_Form_Elements
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnFocus_Click(object sender, EventArgs e)
        {
            int randomTextboxNumber = new Random().Next(1, 3 + 1);
            TextBox textBox = (TextBox)FindControl("TextBox" + randomTextboxNumber);
            textBox.Focus();
        }
    }
}