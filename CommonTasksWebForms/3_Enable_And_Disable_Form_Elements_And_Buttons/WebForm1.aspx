﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="CommonTasksWebForms._3_Enable_And_Disable_Form_Elements_And_Buttons.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:TextBox ID="txtDateTime" runat="server"></asp:TextBox>
            <asp:Button ID="btnGetDateTime" runat="server" Text="Get Date/Time" OnClick="btnGetDateTime_Click" />
        </div>
        <div>
            <asp:Button ID="btnEnableDisableControls" runat="server" Text="Enable/Disable Controls" OnClick="btnEnableDisableControls_Click" />
        </div>
    </form>
</body>
</html>
