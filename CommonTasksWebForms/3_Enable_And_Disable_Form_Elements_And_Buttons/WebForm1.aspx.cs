﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CommonTasksWebForms._3_Enable_And_Disable_Form_Elements_And_Buttons
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnGetDateTime_Click(object sender, EventArgs e)
        {
            txtDateTime.Text = DateTime.Now.ToString();
        }

        protected void btnEnableDisableControls_Click(object sender, EventArgs e)
        {
            if (txtDateTime.Enabled)
            {
                txtDateTime.Enabled = false;
                btnGetDateTime.Enabled = false;
            }
            else
            {
                txtDateTime.Enabled = true;
                btnGetDateTime.Enabled = true;
            }
        }
    }
}