﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CommonTasksWebForms._1_Execute_Code_On_Page_Load
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // this code will only be executed on page load, not on every postback
            if (!IsPostBack)
            {
                lblTime.Text = DateTime.Now.ToString("hh:mm:ss");
            }

            // this code will be executed on every postback eg. button click
            //lblTime.Text = DateTime.Now.ToString("hh:mm:ss");
        }

        protected void btnGetDate_Click(object sender, EventArgs e)
        {
            lblDate.Text = DateTime.Today.ToString();
        }
    }
}