﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="CommonTasksWebForms._1_Execute_Code_On_Page_Load.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            Date: <asp:Label ID="lblDate" runat="server"></asp:Label>
        </div>
        <div>
            Time: <asp:Label ID="lblTime" runat="server"></asp:Label>
        </div>
        <div>
            <asp:Button ID="btnGetDate" runat="server" Text="Get Date" OnClick="btnGetDate_Click" />
        </div>
    </form>
</body>
</html>
